const slider = document.querySelector('.slider-wrapper');
const scrollbar = document.querySelector('.slider-scrollbar');

let isDown = false;
let startX;
let scrollLeft;
let maxScrollLeft;

slider.addEventListener('mousedown', e => {
  isDown = true;
  startX = e.pageX - slider.offsetLeft;
  scrollLeft = slider.scrollLeft;
  e.preventDefault();
});

slider.addEventListener('mouseleave', () => {
  isDown = false;
});

slider.addEventListener('mouseup', () => {
  isDown = false;
});

slider.addEventListener('mousemove', e => {
  if (!isDown) return;
  e.preventDefault();
  const x = e.pageX - slider.offsetLeft;
  const walk = (x - startX) * 2;
  slider.scrollLeft = scrollLeft - walk;
});

slider.addEventListener('scroll', () => {
  const percentage = (slider.scrollLeft / maxScrollLeft) * 100;
  scrollbar.style.width = percentage + '%';
});

window.addEventListener('resize', () => {
  maxScrollLeft = slider.scrollWidth - slider.clientWidth;
});

// infinite scroll
setInterval(() => {
  const firstItem = slider.firstElementChild;
  slider.appendChild(firstItem.cloneNode(true));
  slider.removeChild(firstItem);
}, 3000);