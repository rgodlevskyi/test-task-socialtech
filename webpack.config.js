const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    mode: 'development',
    entry: './src/index.js', // файл, який буде використовуватись як точка входу в проект
    output: {
        filename: 'bundle.js', // назва скомпільованого файлу
        path: path.resolve(__dirname, 'dist') // директорія, в яку буде збережено скомпільований файл
    },
    devServer: {
        static: './src',
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'src/index.html',
        }),
        new MiniCssExtractPlugin({
            filename: 'bundle.css'
        }),
        new CopyWebpackPlugin({
            patterns: [
              { from: 'src/components', to: 'components' },
              { from: 'src/assets', to: 'assets' },
            ],
          }),
    ],
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader'
                ]
            }
        ]
    }
};
